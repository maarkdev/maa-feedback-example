import '../styles/index.scss';
import smoothDnD from './lib/dnd/index.js';
import $ from "cash-dom";
import switches from './switches';

console.log("Feedback Example Started");

let draggingOptions = {
    animationDuration: (switches.elasticAnimation ? 250 : 0),
    groupName: 'eventManagement',
    dragClass: 'contact--dragging',
    dropClass: 'contact--dropping'
};

// Elements
const $attendees = $('.attendees');
const $contactsContainer = $('#contactsContainer');
const $attendeesContainer = $('#attendeesContainer');

// Init Drags
let contactsContainer = smoothDnD($contactsContainer.get(0), 
    Object.assign({

        onDragEnd: onDragEnd,
        onDrop: onDrop,

        onDragStart: function(options) {
            console.log(options);
            
            if ( switches.dropHighlight ) {
                $attendees.addClass('attendees--present-drop-area');
            }
        },

        onDragEnter: function() {

            if ( switches.resizeGhosts ) {
                let $ghost = $('.smooth-dnd-ghost');
                $ghost.width($contactsContainer.width());
            }
        }
    }, 
    draggingOptions)
);
let attendeesContainer = smoothDnD($attendeesContainer.get(0),     
    Object.assign({

        onDragEnd: onDragEnd,
        onDrop: onDrop,
        
        onDragStart: function(options) {            

            setTimeout(function(){
                let $sourceItem = $('.smooth-dnd-draggable-wrapper[style*="visibility"]', $attendeesContainer);
                $sourceItem.addClass('smooth-dnd-source-item');
            }, 0);
        },

        onDragEnter: function() {
            if ( switches.dropHighlight ) {
                $attendees.addClass('attendees--activate-drop-area');
            }
        
            if ( switches.resizeGhosts ) {
                let $ghost = $('.smooth-dnd-ghost');
                $ghost.width($attendeesContainer.width());
            }
        },
        
        onDragLeave: function() {
            if ( switches.dropHighlight ) {
                $attendees.removeClass('attendees--activate-drop-area');
            }
        }
    }, 
    draggingOptions)
);

function onDragEnd(dropResult){
    
    if ( switches.dropHighlight ) {
        $attendees.removeClass('attendees--present-drop-area');
        $attendees.removeClass('attendees--activate-drop-area');
    }
};

function onDrop(dropResult){

    let extension = 0;

    console.log($attendeesContainer.children().length);

    if ( switches.extendEmptyMessage ) {
        extension = 1;
    }

    if ( $attendeesContainer.children().length > extension ) {
        $attendees.addClass('attendees--hide-empty-message');
    } else {
        $attendees.removeClass('attendees--hide-empty-message');
    }
};