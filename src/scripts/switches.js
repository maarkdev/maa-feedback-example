const switches = {
    elasticAnimation : true,
    halfItemOverlap : true,
    dropHighlight : true,
    extendEmptyMessage: true,
    resizeGhosts: true
};

export default switches;